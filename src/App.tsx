/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */
import React from 'react';
import { createAppContainer, createBottomTabNavigator, createStackNavigator } from 'react-navigation';
import { fromLeft } from 'react-navigation-transitions';
import { WeatherPage } from '@components/weather-page';
import { CustomBottomTabBar } from '@components/custom-bottom-tab-bar';
import { StackPage1 } from '@components/stack-page/stack-page-1';
import { StackPage2 } from '@components/stack-page/stack-page-2';
import { StackPage3 } from '@components/stack-page/stack-page-3';
import { FlatListPage } from '@components/flat-list-page';
import { WeatherEnum } from '@enums/weather.enum';
import { Weather } from '@models/weather.model';

const monday: Weather = { dayOfWeek: 'Monday', date: 'January 11 2019', time: '11:41', status: WeatherEnum.Rainy, degree: '28', city: 'Ho Chi Minh', country: 'Viet Nam' };
const tuesday = { dayOfWeek: 'Tuesday', date: 'February 12 2019', time: '12:42', status: WeatherEnum.Snow, degree: '26', city: 'Ho Chi Minh', country: 'Viet Nam' };
const wednesday = { dayOfWeek: 'Wednesday', date: 'May 13 2019', time: '13:43', status: WeatherEnum.Storm, degree: '25', city: 'Ho Chi Minh', country: 'Viet Nam' };
const thursday = { dayOfWeek: 'Thursday', date: 'April 14 2019', time: '14:44', status: WeatherEnum.CloudSnow, degree: '24', city: 'Ho Chi Minh', country: 'Viet Nam' };
const friday = { dayOfWeek: 'Friday', date: 'March 15 2019', time: '15:45', status: WeatherEnum.Sunny, degree: '23', city: 'Ho Chi Minh', country: 'Viet Nam' };
const saturday = { dayOfWeek: 'Saturday', date: 'June 16 2019', time: '16:46', status: WeatherEnum.CloudSun, degree: '22', city: 'Ho Chi Minh', country: 'Viet Nam' };
const sunday = { dayOfWeek: 'Sunday', date: 'July 17 2019', time: '17:47', status: WeatherEnum.Cloud, degree: '21', city: 'Ho Chi Minh', country: 'Viet Nam' };

const CombineNavigator = createStackNavigator(
  {
    StackPage1: {
      screen: StackPage1,
      navigationOptions: () => ({
        title: 'Stack Page 1',
        headerBackTitle: null
      })
    },
    StackPage2: {
      screen: StackPage2,
      navigationOptions: () => ({
        title: 'Stack Page 2',
        headerBackTitle: 'Try'
      })
    },
    StackPage3: StackPage3
  },
  {
    initialRouteName: 'StackPage1',
    transitionConfig: () => fromLeft(),
  },
);

const TabNavigator = createBottomTabNavigator(
  {
    Mon: () => <WeatherPage model={monday}/>,
    Tue: () => <WeatherPage model={tuesday}/>,
    Wed: () => <WeatherPage model={wednesday}/>,
    Thur: () => <WeatherPage model={thursday}/>,
    Fri: () => <WeatherPage model={friday}/>,
    Sat: () => <WeatherPage model={saturday}/>,
    Sun: () => <WeatherPage model={sunday}/>,
    Combine: CombineNavigator,
    FlatList: FlatListPage
  },
  {
    tabBarComponent: (props: any) => <CustomBottomTabBar {...props} />
  }
);

const App = createAppContainer(TabNavigator);

export default App;
