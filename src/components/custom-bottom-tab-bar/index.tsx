import React from 'react';
import {BottomTabBar} from 'react-navigation-tabs';
import styles from '@components/custom-bottom-tab-bar/custom-bottom-tab-bar.style';

export const CustomBottomTabBar = (props: any) => {
    return (
        <BottomTabBar
            {...props}
            activeTintColor='red'
            activeBackgroundColor='#CDCDCD'
            style={styles.container}
            labelStyle={styles.title}
        />
    );
};
