import {StyleSheet, ViewStyle, TextStyle} from 'react-native';

interface BottomTabBarStyle {
    container: ViewStyle;
    title: TextStyle;
}

export default StyleSheet.create<BottomTabBarStyle>({
    container: {
        backgroundColor: '#313024',
        borderTopColor: '#605F60'
    },
    title: {
        color: 'white',
        fontSize: 16,
        paddingBottom: 10
    }
});
