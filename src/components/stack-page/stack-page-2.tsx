import React, { useState } from 'react';
import { SafeAreaView, Text, Button } from 'react-native';

export const StackPage2 = ({navigation}: any) => {
    const [ myNumber, setMyNumber ] = useState(0);

    const gotoPage3 = () => {
        setMyNumber(myNumber + 1);
        console.debug('go to page 3 - DEBUG - myNumber: ' + myNumber);

        navigation.navigate('StackPage3', {
            itemId: 86,
            myNumber: myNumber,
            otherParam: 'Anything you want here'});
    };

    return (
        <SafeAreaView>
            <Text style={{fontSize:30, fontFamily:'Lato-Italic'}}>Stack Page 2 - MyNumber: {myNumber}</Text>
            <Button title='Go to Page 3' onPress={gotoPage3}/>
        </SafeAreaView>
    );
};
