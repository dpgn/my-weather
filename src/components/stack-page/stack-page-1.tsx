import React from 'react';
import { SafeAreaView, Text, Button } from 'react-native';

export const StackPage1 = ({navigation}: any) => {
    return (
        <SafeAreaView>
            <Text style={{fontSize:30, fontFamily:'Lato-Italic'}}>Stack Page 1</Text>
            <Button title='Go to Page 2' onPress={() => navigation.navigate('StackPage2')}/>
        </SafeAreaView>
    );
};
