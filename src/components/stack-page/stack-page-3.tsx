import React from 'react';
import { SafeAreaView, Text, TextInput, View } from 'react-native';

export const StackPage3 = ({navigation}: any) => {
    return (
        <SafeAreaView>
            <Text style={{fontSize:30, fontFamily:'Lato-Italic'}}>Stack Page 3</Text>
            <Text>Param itemId: {navigation.getParam('itemId')}</Text>
            <Text>Param otherParam: {navigation.getParam('otherParam')}</Text>
            <Text>Param is not existed: {navigation.getParam('notExisting', 'this is default value')}</Text>
            <View style={{flexDirection: 'row'}}>
                <Text style={{backgroundColor:'green', paddingTop:14}}>Param myNumber:</Text>
                <TextInput style={{backgroundColor:'orange', flex:1}}>{navigation.getParam('myNumber')}</TextInput>
            </View>
        </SafeAreaView>
    );
};
