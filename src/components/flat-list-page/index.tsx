import React, { useState, useCallback } from 'react';
import { TouchableOpacity, StyleSheet, Text } from 'react-native';
import { SafeAreaView, FlatList } from 'react-navigation';

function generateData() {
    let count = 20;
    let list = [];
    for (let i = 1; i <= count; i++) {
        list.push({id: i, name: `Item ${i}`});
    }
    return list;
}

const DATA = generateData();

const Item = ({ id, title, selected, onSelect }) => {
    return (
        <TouchableOpacity
            onPress={() => onSelect(id)}
            style={[
                styles.item,
                { backgroundColor: selected ? '#16825D' : '#23D18B' },
            ]}
        >
            <Text style={styles.title}>{title}</Text>
        </TouchableOpacity>
    );
}

export const FlatListPage = () => {
    const [selected, setSelected] = useState(new Map());

    const onSelect = useCallback(
        id => {
            const newSelected = new Map(selected);
            newSelected.set(id, !selected.get(id));

            setSelected(newSelected);
        },
        [selected]
    );

    return (
        <SafeAreaView style={styles.container}>
            <FlatList
                data={DATA}
                renderItem={({ item }) => (
                    <Item
                        id={item.id}
                        title={item.name}
                        selected={!!selected.get(item.id)}
                        onSelect={onSelect}
                    />
                )}
                keyExtractor={item => item.id.toString()}
                extraData={selected}
            />
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 10,
    },
    item: {
        backgroundColor: '#f9c2ff',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
    },
    title: {
        fontSize: 32,
    },
});

