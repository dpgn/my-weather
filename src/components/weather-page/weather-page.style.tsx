import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    defaultView: {
        flex:1
    },
    defaultForeColor: {
        color:'white'
    },

    iconBar: {
        alignItems: 'flex-end',
        flexDirection:'row-reverse',
        padding:10
    },

    date: {
        fontSize: 14,
        color:'white'
    },
    dateTimeSeparator:{
        width: '50%',
        height: 3,
        backgroundColor: 'white',
        marginTop:5,
        marginBottom:5
    },
    time: {
        fontSize: 28,
        color:'white'
    },
    degree: {
        fontSize: 40,
        color:'white',
        fontWeight: 'normal'
    },
    status: {
        alignSelf:'center',
        color:'white',
        fontSize:20,
        marginTop: 10,
        marginBottom: 40
    },
    wetUnitBar:{
        borderColor: 'white',
        borderWidth: 1,
        borderRightWidth: 0
    },
    wetUnitBarItem:{
        width: '50%',
        height: 10,
        backgroundColor: 'white'
    },
    wetUnitText: {
        alignSelf:'center',
        color:'white',
        fontSize:16,
        marginTop: 10,
        marginBottom: 5
    },
    cityCountry: {
        fontSize: 22,
        color:'white'
    },

    containerDateTime:{
        paddingLeft:20
    },
    containerBigIcon:{
        alignItems: 'center',
        flex: 1,
        justifyContent: 'space-between',
        paddingTop:20
    },
    containerCityCountry:{
        alignItems: 'flex-end',
        paddingBottom:20,
        paddingRight:20
    },

    sunny: {
        backgroundColor: '#F15A29'
    },
    cloudy: {
        backgroundColor: '#2BB673'
    },
    rainy: {
        backgroundColor: '#00A79D'
    },
    snow: {
        backgroundColor: '#25A8C1'
    },
    storm: {
        backgroundColor: '#1176B5'
    },
    cloudSun: {
        backgroundColor: '#D7DF23'
    },
    cloudSnow: {
        backgroundColor: '#1C95A5'
    },

    linearGradient: {
        flex: 1,
        // paddingLeft: 15,
        // paddingRight: 15,
        // borderRadius: 5
    }
});

export default styles;
