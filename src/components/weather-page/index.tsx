import React, { useState } from 'react';
import { View, Text } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import LinearGradient from 'react-native-linear-gradient';
import styles from '@components/weather-page/weather-page.style';
import { WeatherEnum } from '@enums/weather.enum';
import { LinearGradientColor } from '@models/linear-gradient-color.model';
import { SafeAreaView, ScrollView } from 'react-navigation';

export const WeatherPage = (props: any) => {
    const [model, setModel] = useState(props.model);
    let iconStatusName;
    let bgStyle: LinearGradientColor;

    switch (model.status) {
        case WeatherEnum.Rainy:
            iconStatusName = 'cloud-rain';
            bgStyle = {color1: '#00A79D', color2: '#00A79D', color3: '#039385'};
            break;
        case WeatherEnum.Cloud:
            iconStatusName = 'cloud';
            bgStyle = {color1: '#2BB673', color2: '#2BB673', color3: '#25A565'};
            break;
        case WeatherEnum.Snow:
            iconStatusName = 'slack';
            bgStyle = {color1: '#25A8C1', color2: '#25A8C1', color3: '#1C95A5'};
            break;
        case WeatherEnum.Storm:
            iconStatusName = 'cloud-lightning';
            bgStyle = {color1: '#1176B5', color2: '#1176B5', color3: '#0D638E'};
            break;
        case WeatherEnum.CloudSun:
            iconStatusName = 'cloud-off';
            bgStyle = {color1: '#D7DF23', color2: '#D7DF23', color3: '#8DC63F'};
            break;
        case WeatherEnum.CloudSnow:
            iconStatusName = 'cloud-snow';
            bgStyle = {color1: '#BE8DF3', color2: '#BE8DF3', color3: '#8F53CC'};
            break;
        default:
            iconStatusName = 'sun';
            bgStyle = {color1: '#F15A29', color2: '#F15A29', color3: '#DD441B'};
            break;
    }

    return (
        <SafeAreaView style={[styles.defaultView]} forceInset={{ bottom: 'never' }}>
            <ScrollView>
                <LinearGradient
                    start={{x: 0.0, y: 0.25}} end={{x: 0.5, y: 1.0}}
                    locations={[0,0.5,0.6]}
                    colors={[bgStyle.color1, bgStyle.color2, bgStyle.color3]}
                    style={styles.linearGradient}>
                    <View style={styles.iconBar}>
                        <Icon name="more-vertical" size={20} style={styles.defaultForeColor} />
                        <Icon name="clock" size={20} style={styles.defaultForeColor} />
                        <Text>  </Text>
                        <Icon name="settings" size={20} style={styles.defaultForeColor} />
                    </View>

                    <View style={styles.containerDateTime}>
                        <Text style={styles.cityCountry}>{model.dayOfWeek}</Text>
                        <Text style={styles.date}>{model.date}</Text>
                        <View style={styles.dateTimeSeparator}/>
                        <Text style={styles.time}>{model.time}</Text>
                    </View>

                    <View style={styles.containerBigIcon}>
                        <Icon name={iconStatusName} size={150} style={styles.defaultForeColor} />
                        <Text style={styles.degree}>{model.degree}°C</Text>
                    </View>

                    <Text style={styles.status}>{model.status}</Text>

                    <View style={styles.wetUnitBar}>
                        <View style={styles.wetUnitBarItem}/>
                    </View>

                    <Text style={styles.wetUnitText}>-1°C/+1 c°</Text>

                    <View style={styles.containerCityCountry}>
                        <Text style={styles.cityCountry}>{model.city}</Text>
                        <Text style={styles.cityCountry}>{model.country}</Text>
                    </View>
                </LinearGradient>
            </ScrollView>
        </SafeAreaView>
    );
};
