export interface Weather {
    dayOfWeek: string;
    date: string;
    time: string;
    status: string;
    degree: number;
    city: string;
    country: string;
}
