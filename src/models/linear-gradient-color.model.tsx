export interface LinearGradientColor {
    color1: string;
    color2: string;
    color3: string;
}
