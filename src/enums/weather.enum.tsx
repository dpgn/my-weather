export enum WeatherEnum {
    Sunny='Sunny',
    Cloud='Cloud',
    Rainy='Rainy',
    Snow='Snow',
    Storm='Storm',
    CloudSun='CloudSun',
    CloudSnow='CloudSnow'
}