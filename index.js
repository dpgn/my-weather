/**
 * @format
 */
import 'core-js/es/symbol';// resolve special characters in alias of babel-plugin-module-resolver
import {AppRegistry} from 'react-native';
import App from './src/App';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
