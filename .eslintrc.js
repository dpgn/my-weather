module.exports = {
  root: true,
  extends: '@react-native-community',
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint', 'react', 'react-native'],
  rules: {
    'prettier/prettier': 0,
    'comma-dangle': 'off',
    'react-native/no-inline-styles': 0,
    'jsx-quotes': 0
  }
};
